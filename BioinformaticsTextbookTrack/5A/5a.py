import sys

class Problem():
    def __init__(self, money, coins):
        self.best_num_coins = [0] * (money + 1)
        self.money = money
        self.coins = coins

    def change(self):
        for m in range(1, self.money + 1):
            self.best_num_coins[m] = self.money
            for coin in self.coins:
                if m < coin:
                    continue
                if self.best_num_coins[m-coin] + 1 >= self.best_num_coins[m]:
                    continue
                self.best_num_coins[m] = self.best_num_coins[m-coin] + 1
        print(self.best_num_coins[self.money])

def main():
    money = int(sys.stdin.readline().strip())
    coins = list(map(int, sys.stdin.readline().strip().split(',')))
    problem = Problem(money, coins)
    problem.change()

if __name__ == '__main__':
    sys.exit(main())
