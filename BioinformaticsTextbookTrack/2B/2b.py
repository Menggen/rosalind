import sys

def encode(rna):
    rna_codon_table = {
        'UUU': 'F'   , 'CUU': 'L', 'AUU': 'I', 'GUU': 'V',
        'UUC': 'F'   , 'CUC': 'L', 'AUC': 'I', 'GUC': 'V',
        'UUA': 'L'   , 'CUA': 'L', 'AUA': 'I', 'GUA': 'V',
        'UUG': 'L'   , 'CUG': 'L', 'AUG': 'M', 'GUG': 'V',
        'UCU': 'S'   , 'CCU': 'P', 'ACU': 'T', 'GCU': 'A',
        'UCC': 'S'   , 'CCC': 'P', 'ACC': 'T', 'GCC': 'A',
        'UCA': 'S'   , 'CCA': 'P', 'ACA': 'T', 'GCA': 'A',
        'UCG': 'S'   , 'CCG': 'P', 'ACG': 'T', 'GCG': 'A',
        'UAU': 'Y'   , 'CAU': 'H', 'AAU': 'N', 'GAU': 'D',
        'UAC': 'Y'   , 'CAC': 'H', 'AAC': 'N', 'GAC': 'D',
        'UAA': 'Stop', 'CAA': 'Q', 'AAA': 'K', 'GAA': 'E',
        'UAG': 'Stop', 'CAG': 'Q', 'AAG': 'K', 'GAG': 'E',
        'UGU': 'C'   , 'CGU': 'R', 'AGU': 'S', 'GGU': 'G',
        'UGC': 'C'   , 'CGC': 'R', 'AGC': 'S', 'GGC': 'G',
        'UGA': 'Stop', 'CGA': 'R', 'AGA': 'R', 'GGA': 'G',
        'UGG': 'W'   , 'CGG': 'R', 'AGG': 'R', 'GGG': 'G', 
    }
    protein = []
    for i in range(0, len(rna), 3):
        if rna_codon_table[rna[i:i+3]] == 'Stop':
            break
        else:
           protein.append(rna_codon_table[rna[i:i+3]])
    return ''.join(protein)

def rna(dna):
    return dna.replace("T", "U")

def revc(dna):
    return dna.replace("A", "a").replace("T", "A").replace("a", "T").replace("C", "c").replace("G", "C").replace("c", "G")[::-1]

def main():
    dna, peptide = [line.strip() for line in sys.stdin.readlines()]
    peptide_len = len(peptide)
    for i in range(len(dna) - peptide_len * 3 + 1):
        x = dna[i:i + peptide_len * 3]
        if encode(rna(x)) == peptide:
            print(x)
        if encode(rna(revc(x))) == peptide:
            print(x)

if __name__ == '__main__':
    sys.exit(main())
