import sys

def inverse_burrows_wheeler_transform(r):
    table = [''] * len(r)
    for i in range(len(r)):
        table = sorted(r[j] + table[j] for j in range(len(r)))
    return [row for row in table if row.endswith('$')][0]

def burrows_wheeler_transform(s):
    table = sorted(s[i:] + s[:i] for i in range(len(s)))
    last_column = [row[-1:] for row in table]
    return ''.join(last_column)

def main():
    transform = sys.stdin.readline().strip()
    text = inverse_burrows_wheeler_transform(transform)
    assert(burrows_wheeler_transform(text) == transform)
    print(text)
    
if __name__ == '__main__':
    sys.exit(main())
