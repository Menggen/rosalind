import sys

def de_bruijn_sequence(k, n):
    a = [0] * k * n
    sequence = []
    def db(t, p):
        if t > n:
            if n % p == 0:
                for j in range(1, p + 1):
                    sequence.append(a[j])
        else:
            a[t] = a[t - p]
            db(t + 1, p)
            for j in range(a[t - p] + 1, k):
                a[t] = j
                db(t + 1, t)
    db(1, 1)
    return ''.join(map(str, sequence))

def main():
    k = int(sys.stdin.readline().strip())
    print(de_bruijn_sequence(2, k))

if __name__ == '__main__':
    sys.exit(main())
