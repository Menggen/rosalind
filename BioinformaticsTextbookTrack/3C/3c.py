import itertools
import sys

def read_dataset():
    dataset = [line.strip() for line in sys.stdin.readlines()]
    dna = dataset[0]
    k = int(dataset[1])
    profile_matrix = [list(map(float, data.split())) for data in dataset[2:]]
    return dna, k, profile_matrix

class Problem():
    def __init__(self, dna, k, profile_matrix):
        self.dna = dna
        self.k = k
        self.profile_matrix = profile_matrix
        self.profile_map = {}

        for c, pos in zip('ACGT', range(4)):
            self.profile_map[c] = pos

    def profile_most_motif(self):
        most_profile_motif_so_far = None
        most_profile_so_far = 0.0
        for i in range(len(self.dna) - self.k + 1):
            motif = self.dna[i:i+self.k]
            curr_profile = self.get_profile(motif)
            if curr_profile > most_profile_so_far:
                most_profile_motif_so_far = motif
                most_profile_so_far = curr_profile
        return most_profile_motif_so_far

    def get_profile(self, motif):
        rv = 0.0
        for i in range(self.k):
            rv += self.profile_matrix[i][self.profile_map[motif[i]]]
        return rv

def main():
    dna, k, profile_matrix = read_dataset()
    problem = Problem(dna, k, profile_matrix)
    print(problem.profile_most_motif())

if __name__ == '__main__':
    sys.exit(main())
