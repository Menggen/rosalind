import sys

class GreedyReversalSort():
    def sort(self, permutation_literal):
        permutation = self._parse(permutation_literal)
        unsigned_permutation = [abs(x) for x in permutation]
        for i in range(len(permutation)):
            if unsigned_permutation[i] != i+1:
                j = unsigned_permutation.index(i+1)
                permutation = permutation[:i] \
                        + [-x for x in permutation[i:j+1][::-1]] \
                        + permutation[j+1:]
                unsigned_permutation = unsigned_permutation[:i] \
                        + unsigned_permutation[i:j+1][::-1] \
                        + unsigned_permutation[j+1:]
                print(self._serialize(permutation))
            if permutation[i] == -(i+1):
                permutation[i] = i+1
                print(self._serialize(permutation))
                
    def _parse(self, permutation_literal):
        return list(map(int, permutation_literal[1:-1].split()))
        
    def _serialize(self, permutation):
        permutation_literal = '('
        for i in range(len(permutation)):
            if i != 0:
                permutation_literal += ' '
            entry = permutation[i]
            if entry > 0:
                permutation_literal += '+'
            permutation_literal += str(entry)
        permutation_literal += ')'
        return permutation_literal
        
def main():
    permutation_literal = sys.stdin.readline().strip()
    problem = GreedyReversalSort()
    problem.sort(permutation_literal)
    
if __name__ == '__main__':
    sys.exit(main())
