import itertools
import sys

def overlap_graph(patterns):
    for u, v in itertools.permutations(patterns, 2):
        if u[1:] == v[:-1]:
            print(u, '->', v)

def main():
    patterns = [line.strip() for line in sys.stdin.readlines()]
    overlap_graph(patterns)

if __name__ == '__main__':
    sys.exit(main())
