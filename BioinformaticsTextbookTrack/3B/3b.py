import itertools
import sys

def read_dataset():
    dataset = [line.strip() for line in sys.stdin.readlines()]
    k = int(dataset[0])
    dna_collection = dataset[1:]
    return k, dna_collection

def get_median_string(k, dna_collection):
    best_pattern_so_far = None
    best_d_so_far = k * len(dna_collection)
    for motif in motif_iterator(k):
        d = sum([get_min_d(dna, motif) for dna in dna_collection])
        if d < best_d_so_far:
            best_pattern_so_far = motif
            best_d_so_far = d
    return best_pattern_so_far

def motif_iterator(k):
    for i in itertools.product('ACGT', repeat=k):
        yield ''.join(i)

def get_min_d(dna, motif):
    motif_len = len(motif)
    min_d_so_far = motif_len
    for i in range(len(dna) - motif_len + 1):
        count = 0
        dna_piece = dna[i:i+motif_len]
        for j in range(motif_len):
            if dna_piece[j] == motif[j]:
                count += 1
        min_d_so_far = min(min_d_so_far, motif_len - count)
    return min_d_so_far

def main():
    k, dna_collection = read_dataset()
    print(get_median_string(k, dna_collection))

if __name__ == '__main__':
    sys.exit(main())
