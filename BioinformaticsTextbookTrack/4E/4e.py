import sys

class Problem():
    def __init__(self):
        self.graph = {}

    def solve(self):
        self.read_dataset()
        print('->'.join(self.eulerian_cycle()))
    
    def read_dataset(self):
        for line in sys.stdin.readlines():
            tokens = line.strip().split()
            assert(len(tokens) == 3)
            assert(tokens[1] == '->')
            self.graph[tokens[0]] = set(tokens[2].split(','))

    def eulerian_cycle(self):
        visited_edges = {}
        cycle, visited_edges = self.walk_randomly(visited_edges, '0')
        while True:
            edge = self.get_unvisited_edge(visited_edges)
            if not edge:
                break
            pos = cycle.index(edge[0])
            new_cycle, visited_edges = self.walk_randomly(visited_edges, edge[0])
            cycle = cycle[pos:] + cycle[1:pos] + new_cycle
        return cycle
    
    def walk_randomly(self, visited_edges, initial_node):
        curr_node = initial_node
        cycle = [curr_node]
        while True:
            found = False
            for next_node in self.graph[curr_node]:
                if curr_node not in visited_edges:
                    visited_edges[curr_node] = set()
                if next_node not in visited_edges[curr_node]:
                    visited_edges[curr_node].add(next_node)
                    curr_node = next_node
                    cycle.append(curr_node)
                    found = True
                    break
            if not found:
                break
        return cycle, visited_edges

    def get_unvisited_edge(self, visited_edges):
        for u in self.graph:
            if u in visited_edges:
                for v in (self.graph[u] - visited_edges[u]):
                    return (u, v)
        return None

def main():
    problem = Problem()
    problem.solve()

if __name__ == '__main__':
    sys.exit(main())
