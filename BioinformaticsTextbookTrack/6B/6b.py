import sys

class BreakpointCountProblem():
    def get(self, permutation_literal):
        permutation = self._parse(permutation_literal)
        count = 0
        leftmost = 0
        for i in range(len(permutation)):
            a = permutation[i]
            if a != leftmost + 1:
                count += 1
            leftmost = a
        if len(permutation) != leftmost:
            count += 1
        return count

    def _parse(self, permutation_literal):
        return list(map(int, permutation_literal[1:-1].split()))
        
def main():
    permutation_literal = sys.stdin.readline().strip()
    problem = BreakpointCountProblem()
    print(problem.get(permutation_literal))
    
if __name__ == '__main__':
    sys.exit(main())
