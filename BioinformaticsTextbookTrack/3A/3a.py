import itertools
import sys

def read_dataset():
    dataset = [line.strip() for line in sys.stdin.readlines()]
    k, d = list(map(int, dataset[0].split()))
    dna_collection = dataset[1:]
    return k, d, dna_collection
   
def is_matched(dna_collection, motif, d):
    motif_len = len(motif)
    for dna in dna_collection:
        matched = False
        for i in range(len(dna) - motif_len + 1):
            count = 0
            dna_piece = dna[i:i+motif_len]
            for j in range(motif_len):
                if dna_piece[j] == motif[j]:
                    count += 1
            if motif_len - count <= d:
                matched = True
                break
        if not matched:
            return False
    return True
    
def main():
    k, d, dna_collection = read_dataset()
    rv = []
    for i in itertools.product('ACGT', repeat=k):
        motif = ''.join(i)
        if is_matched(dna_collection, motif, d):
            rv.append(motif)
    print(' '.join(rv))
    
if __name__ == '__main__':
    sys.exit(main())
