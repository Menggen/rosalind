import sys

class Problem():
    def __init__(self):
        self.graph = {}

    def solve(self):
        self.read_dataset()
        path = self.eulerian_path()
        rv = path[0]
        for i in range(1, len(path)):
            rv += path[i][-1]
        print(rv)
    
    def read_dataset(self):
        for line in sys.stdin.readlines():
            tokens = line.strip().split()
            assert(len(tokens) == 3)
            assert(tokens[1] == '->')
            self.graph[tokens[0]] = set(tokens[2].split(','))

    def eulerian_path(self):
        u, v = self.get_missing_edge()
        if u not in self.graph:
            self.graph[u] = set()
        self.graph[u].add(v)
        cycle = self.eulerian_cycle()[:-1]
        for i in range(len(cycle)):
            possible_path = cycle[i:] + cycle[:i]
            if possible_path[0] == v and possible_path[-1] == u:
                return possible_path

    def get_missing_edge(self):
        indegree_map = {}
        outdegree_map = {}
        for u in self.graph:
            outdegree_map[u] = len(self.graph[u])
            for v in self.graph[u]:
                if v not in indegree_map:
                    indegree_map[v] = 0
                indegree_map[v] += 1
        initial_node = self.get_initial_node(indegree_map, outdegree_map)
        terminal_node = self.get_terminal_node(indegree_map, outdegree_map)
        return terminal_node, initial_node

    def get_initial_node(self, indegree_map, outdegree_map):
        for u in outdegree_map:
            if u not in indegree_map:
                return u
            if indegree_map[u] < outdegree_map[u]:
                return u
        return None

    def get_terminal_node(self, indegree_map, outdegree_map):
        for v in indegree_map:
            if v not in outdegree_map:
                return v
            if indegree_map[v] > outdegree_map[v]:
                return v
        return None

    def eulerian_cycle(self):
        visited_edges = {}
        cycle, visited_edges = self.walk_randomly(visited_edges, self.get_node_randomly())
        while True:
            edge = self.get_unvisited_edge(visited_edges)
            if not edge:
                break
            pos = cycle.index(edge[0])
            new_cycle, visited_edges = self.walk_randomly(visited_edges, edge[0])
            cycle = cycle[pos:] + cycle[1:pos] + new_cycle
        return cycle
    
    def walk_randomly(self, visited_edges, initial_node):
        curr_node = initial_node
        cycle = [curr_node]
        while True:
            found = False
            for next_node in self.graph[curr_node]:
                if curr_node not in visited_edges:
                    visited_edges[curr_node] = set()
                if next_node not in visited_edges[curr_node]:
                    visited_edges[curr_node].add(next_node)
                    curr_node = next_node
                    cycle.append(curr_node)
                    found = True
                    break
            if not found:
                break
        return cycle, visited_edges

    def get_node_randomly(self):
        for u in self.graph:
            return u

    def get_unvisited_edge(self, visited_edges):
        for u in self.graph:
            if u in visited_edges:
                for v in (self.graph[u] - visited_edges[u]):
                    return (u, v)
        return None

def main():
    problem = Problem()
    problem.solve()

if __name__ == '__main__':
    sys.exit(main())
