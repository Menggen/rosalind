import sys

def build_graph(k, text):
    graph = {}
    for i in range(len(text) - k + 1):
        u, v = text[i:i+k-1], text[i+1:i+k]
        if u not in graph:
            graph[u] = []
        graph[u].append(v)
    for u in graph:
        print(u, '->', ','.join(graph[u]))

def main():
    k_literal, text = [line.strip() for line in sys.stdin.readlines()]
    build_graph(int(k_literal), text)

if __name__ == '__main__':
    sys.exit(main())
