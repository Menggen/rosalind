import sys

def burrows_wheeler_transform(s):
    table = sorted(s[i:] + s[:i] for i in range(len(s)))
    last_column = [row[-1:] for row in table]
    return ''.join(last_column)

def main():
    text = sys.stdin.readline().strip()
    print(burrows_wheeler_transform(text))
    
if __name__ == '__main__':
    sys.exit(main())
