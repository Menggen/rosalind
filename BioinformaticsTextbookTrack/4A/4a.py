import sys

def composition(k, text):
    rv = []
    for i in range(len(text) - k + 1):
        rv.append(text[i:i+k])
    rv.sort()
    print('\n'.join(rv))

def main():
    k_literal, text = [line.strip() for line in sys.stdin.readlines()]
    composition(int(k_literal), text)

if __name__ == '__main__':
    sys.exit(main())
