import sys

def build_graph(patterns):
    graph = {}
    for pattern in patterns:
        u, v = pattern[:-1], pattern[1:]
        if u not in graph:
            graph[u] = []
        graph[u].append(v)
    for u in graph:
        print(u, '->', ','.join(graph[u]))

def main():
    patterns = [line.strip() for line in sys.stdin.readlines()]
    build_graph(patterns)

if __name__ == '__main__':
    sys.exit(main())
