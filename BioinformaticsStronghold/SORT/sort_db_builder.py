import ast
import itertools
import sqlite3
import sys

class DistanceMapDatabaseBuilder():
    def __init__(self, n):
        self.db_file = 'DistanceMapData.db'
        self.n = n
        self.records_in_memory = []
        self.sync_count = 0
    
    def build(self):
        self.create_table()
        self.init_distance_map()
    
    def create_table(self):
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS DistanceMap(Permutation text NOT NULL, Sorting text NOT NULL, Distance int NOT NULL, UNIQUE(Permutation) ON CONFLICT IGNORE);''')
        c.execute('''CREATE INDEX IF NOT EXISTS DistanceMap_Permutation_Index ON DistanceMap(Permutation);''')
        c.execute('''CREATE INDEX IF NOT EXISTS DistanceMap_Distance_Index ON DistanceMap(Distance);''')
        conn.commit()
        conn.close()
        
    def init_distance_map(self):
        initial_permutation = [_ for _ in range(1, self.n+1)]
        self.insert_record(initial_permutation, [])
        for permutation, sorting in self.reversal_generator(initial_permutation, []):
            self.insert_record(permutation, sorting)
        self.sync_records()
        for i in range(2, self.n):
            self.update_records(i)
            self.sync_records()
    
    def update_records(self, distance):
        print('update_records =>', distance)
        assert(distance > 1)
        for permutation, sorting in self.fetch_records_by_distance(distance-1):
            for next_permutation, next_sorting in self.reversal_generator(permutation, sorting):
                self.insert_record(next_permutation, next_sorting)
    
    def reversal_generator(self, permutation, sorting):
        for i, j in itertools.combinations(range(self.n), 2):
            next_permutation = permutation[:i] + permutation[i:j+1][::-1] + permutation[j+1:]
            next_sorting = list(sorting) + [(i+1, j+1)]
            yield next_permutation, next_sorting
    
    def insert_record(self, permutation, sorting):
        self.records_in_memory.append(((str(permutation), str(sorting), len(sorting))))
        if len(self.records_in_memory) < 20000:
            return
        self.sync_records()
        
    def sync_records(self):
        self.sync_count += 1
        if self.sync_count % 10 == 0:
            print('sync_records count =>', self.sync_count)
        if not self.records_in_memory:
            return
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        c.executemany('INSERT INTO DistanceMap VALUES (?,?,?)', self.records_in_memory)
        conn.commit()
        conn.close()
        self.records_in_memory = []
    
    def fetch_records_by_distance(self, distance):
        rv = []
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        for permutation, sorting in c.execute('SELECT Permutation, Sorting FROM DistanceMap WHERE distance=?', (distance, )):
            rv.append((ast.literal_eval(permutation), ast.literal_eval(sorting)))
        conn.commit()
        conn.close()
        return rv
    
def main():
    builder = DistanceMapDatabaseBuilder(10)
    builder.build()
    
if __name__ == '__main__':
    sys.exit(main())
