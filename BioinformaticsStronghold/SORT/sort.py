import ast
import sqlite3
import sys

def read_permutation_pair():
    p = list(map(int, sys.stdin.readline().strip().split()))
    q = list(map(int, sys.stdin.readline().strip().split()))
    return p, q

class DistanceMapDatabase():
    def __init__(self, n):
        self.db_file = 'DistanceMapData.db'
        self.n = n
        
    def query(self, pair):
        permutation = self.normalize(pair)
        return self.fetch_record_by_permutation(permutation)
        
    def normalize(self, pair):
        p, q, r = pair[0], pair[1], []
        for i in range(len(q)):
            for j in range(len(p)):
                if p[j] == q[i]:
                    r.append(j+1)
                    break
        return r
    
    def fetch_record_by_permutation(self, permutation):
        conn = sqlite3.connect(self.db_file)
        c = conn.cursor()
        c.execute('SELECT Sorting, Distance FROM DistanceMap WHERE Permutation=?', (str(permutation), ))
        sorting, distance = c.fetchone()
        conn.commit()
        conn.close()
        return ast.literal_eval(sorting), distance
    
class ReversalSorting():
    def __init__(self, n):
        self.database = DistanceMapDatabase(n)
    
    def sort(self, pair):
        return self.database.query(pair)
    
def main():
    sorting = ReversalSorting(10)
    pair = read_permutation_pair()
    sorting, distance = sorting.sort(pair)
    print(distance)
    for reversal in sorting:
        print(' '.join(map(str, reversal)))
    
if __name__ == '__main__':
    sys.exit(main())
